### This Package and it's Document will be more helpful in near future. (Developing...)
<br />



# React Chart

[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://gitlab.com/socit/react-chart/blob/master/LICENSE)
[![npm version](https://img.shields.io/npm/v/@socit/react-chart)](https://www.npmjs.com/package/@socit/react-chart)

React-Chart is a library for creating svg base charts with react.

## Installation
```
$ npm install --save @socit/react-chart
```

## Basic Usage
```jsx
import React from 'react';
import ReactChart from 'react-chart';

import './App.css';

function App() {

    const data = [
      { x: [1398, 6, 1], y: 1 },
      { x: [1398, 6, 2], y: 2 },
      { x: [1398, 6, 3], y: 3 },
      { x: [1398, 6, 4], y: 4 },
      { x: [1398, 6, 5], y: 5 },
      { x: [1398, 6, 6], y: 6 },
      { x: [1398, 6, 8], y: 7 },
      { x: [1398, 6, 9], y: 8 },
      { x: [1398, 6, 10], y: 10 }
    ];

    const data2 = [
      { x: 1, open: 2, close: 3, high: 4, low: 1 },
      { x: 2, open: 3, close: 4, high: 5, low: 2 },
      { x: 3, open: 4, close: 6, high: 7, low: 2 },
      { x: 4, open: 5, close: 3, high: 6, low: 2 },
      { x: 5, open: 4, close: 3, high: 5, low: 1 },
      { x: 6, open: 4, close: 5, high: 6, low: 2 },
      { x: 7, open: 4, close: 5, high: 7, low: 3 },
      { x: 8, open: 5, close: 6, high: 6, low: 4 },
      { x: 9, open: 6, close: 3, high: 7, low: 3 }
  ];

  return (
    <div className="App">
      <ReactChart
        width={700}
        height={300}
        axisX={true}
        axisY={true}
        charts={[
          { type: 'line', color: '#f00', data: data }
        ]}
      />

      <ReactChart
        width={500}
        height={200}
        axisX={true}
        axisY={true}
        charts={[
          { type: 'candlestick', upperColor: '#0f0', lowerColor: '#f00', data: data2 }
        ]}
      />
    </div>
  );
}

export default App;
```

### Documentation

For documentation see [socit.gitlab.io/react-chart](https://socit.gitlab.io/react-chart).

### License

React-Chart is under the terms of [MIT licensed](https://gitlab.com/socit/react-chart/blob/master/License).