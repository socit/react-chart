import React from 'react';

import Line from './components/Line';
import Candlestick from './components/Candlestick';
import AxisX from './components/AxisX';
import AxisY from './components/AxisY';

import {
    DatePlus
} from '../../modules';

import './index.scss';

/**
 * NOTE:
 *  - Date must be array [year, month, day]
 *  - Y cannot get date
 * 
 */

function Chart(props) {
    const {
        width = 400,
        height = 400,
        axisX = true,
        axisY = true,
        charts = []
    } = props;

    // config
    const zoom = 1;
    const axisBarSizeX = axisX ? 20 : 0;
    const axisBarSizeY = axisY ? 25 : 0;
    const minDistanceX = 60;
    const minDistanceY = 20;

    // padding
    const paddingX = 20;
    const paddingY = 20;

    // is date (X)
    let isDate = false;
    if (charts[0] && charts[0].data && charts[0].data[0] && Array.isArray(charts[0].data[0].x)) {
        isDate = true;
    }

    /**
     * TODO:
     *  - Calculate min,max base on `zoom`
     */

    // calculate min and max
    let maxX = 0;
    let minX = 0;
    let maxY = 0;
    let minY = 0;
    if (charts.length > 0) {
        // initial
        if (charts[0].data.length > 0) {
            maxX = charts[0].data[0].x;
            minX = charts[0].data[0].x;
            if (charts[0].type === 'candlestick') {
                maxY = parseInt(charts[0].data[0].high);
                minY = parseInt(charts[0].data[0].low);
            } else {
                maxY = parseInt(charts[0].data[0].y);
                minY = parseInt(charts[0].data[0].y);
            }
        }

        // find
        for (let i = 0; i < charts.length; i++) {
            const type = charts[i].type;
            const data = charts[i].data;

            for (let j = 0; j < data.length; j++) {
                // min,max on X
                if (!isDate) {
                    if (data[j].x > maxX) {
                        maxX = parseInt(data[j].x);
                    }
                    if (data[j].x < minX) {
                        minX = parseInt(data[j].x);
                    }
                } else {
                    if (DatePlus.compare(data[j].x, maxX) === 1) {
                        maxX = data[j].x;
                    }
                    if (DatePlus.compare(data[j].x, minX) === -1) {
                        minX = data[j].x;
                    }
                }
    
                // min,max on Y
                if (type === 'candlestick') {
                    if (data[j].high > maxY) {
                        maxY = parseInt(data[j].high);
                    }
                    if (data[j].low < minY) {
                        minY = parseInt(data[j].low);
                    }
                } else {
                    if (data[j].y > maxY) {
                        maxY = parseInt(data[j].y);
                    }
                    if (data[j].y < minY) {
                        minY = parseInt(data[j].y);
                    }
                }
            }
        }
    }

    // choose chart
    let chartItems = [];
    for (let i = 0; i < charts.length; i++) {
        if (charts[i].type === 'line') {
            chartItems.push(
                <Line
                    key={i}
                    width={width}
                    height={height}
                    paddingX={paddingX}
                    paddingY={paddingY}
                    axisBarSizeX={axisBarSizeX}
                    axisBarSizeY={axisBarSizeY}
                    data={charts[i].data}
                    maxX={maxX}
                    minX={minX}
                    maxY={maxY}
                    minY={minY}
                    isDate={isDate}
                    color={charts[i].color}
                />
            );
        } else if (charts[i].type === 'candlestick') {
            chartItems.push(
                <Candlestick
                    key={i}
                    width={width}
                    height={height}
                    paddingX={paddingX}
                    paddingY={paddingY}
                    axisBarSizeX={axisBarSizeX}
                    axisBarSizeY={axisBarSizeY}
                    data={charts[i].data}
                    maxX={maxX}
                    minX={minX}
                    maxY={maxY}
                    minY={minY}
                    isDate={isDate}
                    upperColor={charts[i].upperColor}
                    lowerColor={charts[i].lowerColor}
                />
            );
        }
    }

    return (
        <div className="chart" style={{ width, height }}>
            {axisX ?
                <AxisX
                    width={width}
                    height={height}
                    paddingX={paddingX}
                    axisBarSizeX={axisBarSizeX}
                    axisBarSizeY={axisBarSizeY}
                    minDistanceX={minDistanceX}
                    maxX={maxX}
                    minX={minX}
                    isDate={isDate}
                />
                :
                null
            }

            {axisY ?
                <AxisY
                    width={width}
                    height={height}
                    paddingY={paddingY}
                    axisBarSizeX={axisBarSizeX}
                    axisBarSizeY={axisBarSizeY}
                    minDistanceY={minDistanceY}
                    maxY={maxY}
                    minY={minY}
                />
                :
                null
            }

            <div className="chart-inside">
                {chartItems}
            </div>
        </div>
    );
}

export default Chart;
