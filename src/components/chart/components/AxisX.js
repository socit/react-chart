import React from 'react';
import PersianDate from 'persian-date';

import './AxisX.scss';

function AxisX(props) {
    const {
        width,
        height,
        paddingX,
        axisBarSizeX,
        axisBarSizeY,
        minDistanceX,
        maxX,
        minX,
        isDate
    } = props;

    const workWidth = width - axisBarSizeY - paddingX;
    const maxNum = Math.round(workWidth / minDistanceX);
    const minNum = 4;

    let duration = 0;
    if (!isDate) {
        duration = maxX - minX;
    } else {
        const maxDate = new PersianDate(maxX);
        const minDate = new PersianDate(minX);
        duration = maxDate.diff(minDate, 'days');
    }

    let num = null;
    let gap = null;
    for (let i = maxNum; i > minNum; i--) {
        const testGap = duration / i;
        if (Number.isInteger(testGap)) {
            num = i;
            gap = testGap;
            break;
        }
    }

    if (num === null) {
        num = maxNum;
        gap = duration / maxNum;
    }

    let textItems = [];
    let lineItems = [];
    for (let i = 0; i <= num; i++) {
        let text = '';
        let left = 0;
        if (!isDate) {
            text = minX + Math.round(i * gap);
            left = (((i * gap) / duration) * workWidth) + axisBarSizeY + (paddingX / 2);
        } else {
            const minDate = new PersianDate(minX).startOf('day');
            const date = minDate.add(Math.round(i * gap), 'days').startOf('day');
            text = `${date.year()}/${date.month()}/${date.date()}`;
            left = (((date.diff(minDate, 'days')) / duration) * workWidth) + axisBarSizeY + (paddingX / 2);
        }

        textItems.push(
            <text
                key={`t-${i}`}
                className="chart-axis-x-item"
                x={left}
                y={height}
                fill="#aaa"
                fontSize={12}
                textAnchor="middle"
            >
                {text}
            </text>
        );

        lineItems.push(
            <line
                key={`l-${i}`}
                x1={left}
                y1={height - axisBarSizeX}
                x2={left}
                y2={0}
                stroke="#e6e6e6"
                strokeWidth={1}
                strokeDasharray="5, 5"
                fill="transparent"
            />
        );
    }

    return (
        <svg
            width={width}
            height={height}
            className="chart-axis-x"
        >
            {lineItems}

            <line
                x1={axisBarSizeY + (paddingX / 2)}
                y1={height - axisBarSizeX}
                x2={width - (paddingX / 2)}
                y2={height - axisBarSizeX}
                stroke="#aaa"
                strokeWidth={1}
                fill="transparent"
            />

            {textItems}
        </svg>
    );
}

export default AxisX;
