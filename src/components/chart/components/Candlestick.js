import React from 'react';
import PersianDate from 'persian-date';

import './Candlestick.scss';

function Candlestick(props) {
    const {
        width,
        height,
        paddingX,
        paddingY,
        axisBarSizeX,
        axisBarSizeY,
        data,
        maxX,
        minX,
        maxY,
        minY,
        isDate,

        upperColor = 'rgb(34 , 180, 178)',
        lowerColor = 'rgb(252, 74 , 96 )'
    } = props;

    const workWidth = width - axisBarSizeY - paddingX;
    const workHeight = height - axisBarSizeX - paddingY;

    let durationX = 0;
    // Y does not support Date
    let durationY = maxY - minY;
    if (!isDate) {
        durationX = maxX - minX;
    } else {
        const maxDate = new PersianDate(maxX);
        const minDate = new PersianDate(minX);
        durationX = maxDate.diff(minDate, 'days');
    }

    const items = [];
    const minDate = !isDate ? null : new PersianDate(minX);
    /**
     * TODO:
     *  - Only show `min < item < max`
     */
    for (let i = 0; i < data.length; i++) {
        const up    = data[i].close > data[i].open ? data[i].close : data[i].open;
        const down  = data[i].close > data[i].open ? data[i].open : data[i].close;
        const color = data[i].close > data[i].open ? upperColor : lowerColor;

        let left = null;
        if (!isDate) {
            left = (((data[i].x - minX) / durationX) * workWidth) + axisBarSizeY + (paddingX / 2);
        } else {
            const date = new PersianDate(data[i].x);
            left = (((date.diff(minDate, 'days')) / durationX) * workWidth) + axisBarSizeY + (paddingX / 2);
        }

        let d = 'M ' + left       + ' ' + (workHeight - (((data[i].low - minY) / durationY) * workHeight) + (paddingY / 2));
        d = d + 'L ' + left       + ' ' + (workHeight - (((down - minY) / durationY) * workHeight) + (paddingY / 2)); // up
        d = d + 'L ' + (left - 5) + ' ' + (workHeight - (((down - minY) / durationY) * workHeight) + (paddingY / 2)); // left
        d = d + 'L ' + (left - 5) + ' ' + (workHeight - (((up - minY) / durationY) * workHeight) + (paddingY / 2)); // up
        d = d + 'L ' + left       + ' ' + (workHeight - (((up - minY) / durationY) * workHeight) + (paddingY / 2)); // right
        d = d + 'L ' + left       + ' ' + (workHeight - (((data[i].high - minY) / durationY) * workHeight) + (paddingY / 2)); // up
        d = d + 'L ' + left       + ' ' + (workHeight - (((up - minY) / durationY) * workHeight) + (paddingY / 2)); // down
        d = d + 'L ' + (left + 5) + ' ' + (workHeight - (((up - minY) / durationY) * workHeight) + (paddingY / 2)); // right
        d = d + 'L ' + (left + 5) + ' ' + (workHeight - (((down - minY) / durationY) * workHeight) + (paddingY / 2)); // down
        d = d + 'L ' + left       + ' ' + (workHeight - (((down - minY) / durationY) * workHeight) + (paddingY / 2)); // left
        items.push(
            <path key={i} d={d} className="chart-candlestick-item" fill={color} stroke={color} />
        );
    }

    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width={width}
            height={height} 
            className="chart-candlestick"
        >
            {items}
        </svg>
    );
}

export default Candlestick;
