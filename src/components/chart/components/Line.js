import React from 'react';
import PersianDate from 'persian-date';

import './Line.scss';

function Line(props) {
    const {
        width,
        height,
        paddingX,
        paddingY,
        axisBarSizeX,
        axisBarSizeY,
        data,
        maxX,
        minX,
        maxY,
        minY,
        isDate,

        color = '#aaa'
    } = props;

    const workWidth = width - axisBarSizeY - paddingX;
    const workHeight = height - axisBarSizeX - paddingY;

    let durationX = 0;
    // Y does not support Date
    let durationY = maxY - minY;
    if (!isDate) {
        durationX = maxX - minX;
    } else {
        const maxDate = new PersianDate(maxX);
        const minDate = new PersianDate(minX);
        durationX = maxDate.diff(minDate, 'days');
    }

    let d = 'M 0 0';
    const minDate = !isDate ? null : new PersianDate(minX);
    /**
     * TODO:
     *  - Only show `min < item < max`
     */
    for (let i = 0; i < data.length; i++) {
        let left = null;
        if (!isDate) {
            left = (((data[i].x - minX) / durationX) * workWidth) + axisBarSizeY + (paddingX / 2);
        } else {
            const date = new PersianDate(data[i].x);
            left = (((date.diff(minDate, 'days')) / durationX) * workWidth) + axisBarSizeY + (paddingX / 2);
        }
        const top = workHeight - (((data[i].y - minY) / durationY) * workHeight) + (paddingY / 2);

        if (i === 0) {
            d = 'M ' + left + ' ' + top;
        } else {
            d = d + ' L ' + left + ' ' + top;
        }
    }

    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width={width}
            height={height}
            className="chart-line"
        >
            <path d={d} stroke={color} fill="transparent"/>
        </svg>
    );
}

export default Line;
