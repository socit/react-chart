import React from 'react';

import './AxisY.scss';

function AxisY(props) {
    const {
        width,
        height,
        paddingY,
        axisBarSizeX,
        axisBarSizeY,
        minDistanceY,
        maxY,
        minY
    } = props;

    const workHeight = height - axisBarSizeX - paddingY;
    const maxNum = Math.round(workHeight / minDistanceY);
    const minNum = 4;

    let duration = maxY - minY;

    let num = null;
    let gap = null;
    for (let i = maxNum; i > minNum; i--) {
        const testGap = duration / i;
        if (Number.isInteger(testGap)) {
            num = i;
            gap = testGap;
            break;
        }
    }

    if (num === null) {
        num = maxNum;
        gap = duration / maxNum;
    }

    let textItems = [];
    let lineItems = [];
    for (let i = 0; i < num + 1; i++) { // +1 for show last item
        let text = minY + Math.round(i * gap);
        let top = (workHeight - (((i * gap) / duration) * workHeight) + (paddingY / 2));

        textItems.push(
            <text
                key={`t-${i}`}
                className="chart-axis-y-item"
                x={0}
                dx={11}
                y={top}
                dy={2}
                fill="#aaa"
                fontSize={9}
                textAnchor="middle"
            >
                {text}
            </text>
        );

        lineItems.push(
            <line
                key={`l-${i}`}
                x1={axisBarSizeY}
                y1={top}
                x2={width}
                y2={top}
                stroke="#e6e6e6"
                strokeWidth={1}
                strokeDasharray="5, 5"
                fill="transparent"
            />
        );
    }

    return (
        <svg
            width={width}
            height={height}
            className="chart-axis-y"
        >
            {lineItems}

            <line
                x1={axisBarSizeY}
                y1={(height - (axisBarSizeX + (paddingY / 2)))}
                x2={axisBarSizeY}
                y2={height - (height - (paddingY / 2))}
                stroke="#aaa"
                strokeWidth={1}
                fill="transparent"
            />

            {textItems}
        </svg>
    );
}

export default AxisY;
