import React from 'react';
import PropTypes from 'prop-types';
import PersianDate from 'persian-date';

import './MonthPicker.scss';

function MonthPicker(props) {
    if (props.gregorian) {
        PersianDate.toCalendar('gregorian');
        PersianDate.toLocale('en');
    }
    const MonthNames = PersianDate.rangeName().months;

    const rowItems = [];
    for (let i = 0; i < 4; i++) {
        const cellItems = [];
        for (let j = 0; j < 3; j++) {
            const isSelected = props.showDate[1] === ((i * 3) + j + 1);
            let style = {};
            if (isSelected) {
                style = { backgroundColor: props.secondaryColor, color: props.primaryColor };
            } else {
                style = { color: props.secondaryColor };
            }

            cellItems.push(
                <div
                    key={`c-${j}`}
                    className={`${props.className}-month-picker-cell ${props.small ? 'small' : ''}`}
                    style={style}
                    onClick={() => {
                        props.onShowDateChange([props.showDate[0], ((i * 3) + j + 1), props.showDate[2]]);
                    }}
                >
                    {MonthNames[(i * 3) + j]}
                </div>
            );
        }

        rowItems.push(
            <div key={`r-${i}`} className={`${props.className}-month-picker-row`} style={{ flexDirection: props.gregorian ? 'row' : 'row-reverse' }}>
                {cellItems}
            </div>
        );
    }

    return (
        <div className={`${props.className}-month-picker`}>
            {rowItems}
        </div>
    );
}

MonthPicker.propTypes = {
    small     : PropTypes.bool,
    gregorian : PropTypes.bool,

    showDate         : PropTypes.array,
    onShowDateChange : PropTypes.func,

    className      : PropTypes.string,
    primaryColor   : PropTypes.string,
    secondaryColor : PropTypes.string
};

export { MonthPicker };
