import React from 'react';
import PropTypes from 'prop-types';

import './YearPicker.scss';

function YearPicker(props) {
    let startYear = 1390, endYear = 1420;
    if (props.gregorian) {
        startYear = 2010, endYear = 2040;
    }
    const items = [];
    for (let i = startYear; i < endYear; i++) {
        const isSelected = props.showDate[0] === i;
        let style = {};
        if (isSelected) {
            style = { backgroundColor: props.secondaryColor, color: props.primaryColor };
        } else {
            style = { color: props.secondaryColor };
        }

        items.push(
            <div
                key={`i-${i}`}
                className={`${props.className}-year-picker-item ${props.small ? 'small' : ''}`}
                style={style}
                onClick={() => {
                    props.onShowDateChange([i, props.showDate[1], props.showDate[2]]);
                }}
            >
                {i}
            </div>
        );
    }

    return (
        <div className={`${props.className}-year-picker`}>
            {items}
        </div>
    );
}

YearPicker.propTypes = {
    small     : PropTypes.bool,
    gregorian : PropTypes.bool,

    showDate         : PropTypes.array,
    onShowDateChange : PropTypes.func,

    className      : PropTypes.string,
    primaryColor   : PropTypes.string,
    secondaryColor : PropTypes.string
};

export { YearPicker };
