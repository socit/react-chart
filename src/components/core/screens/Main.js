import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PersianDate from 'persian-date';

import {
    DatePlus,
    Color
} from '../../../modules';

import './Main.scss';

const DAY_STATE = {
    INACTIVE          : 0,
    NOT_IN_RANGE      : 1,
    IN_RANGE          : 2,
    SELECTED          : 3,
    IN_SELECTED_RANGE : 4,
};

class Main extends Component {
    constructor(props) {
        super(props);

        if (props.gregorian) {
            PersianDate.toCalendar('gregorian');
            PersianDate.toLocale('en');
        }

        this.state = {
            daysRollStatus : 0 // 0: normal, 1: go down, 2: go up
        };

        this.onMainTopMonthPreviousClick = this.onMainTopMonthPreviousClick.bind(this);
        this.onMainTopMonthNextClick = this.onMainTopMonthNextClick.bind(this);
    }

    onMainTopMonthPreviousClick() {
        this.setState({ daysRollStatus: 1 }, () => {
            setTimeout(() => {
                let newDate = null;
                if (this.props.showDate[1] - 1 >= 1) {
                    newDate = [this.props.showDate[0], this.props.showDate[1] - 1, this.props.showDate[2]];
                } else {
                    newDate = [this.props.showDate[0] - 1, 12, this.props.showDate[2]];
                }
                this.setState({ daysRollStatus: 0 });
                this.props.onShowDateChange(newDate);
            }, 300);
        })
    }

    onMainTopMonthNextClick() {
        this.setState({ daysRollStatus: 2 }, () => {
            setTimeout(() => {
                let newDate = null;
                if (this.props.showDate[1] + 1 <= 12) {
                    newDate = [this.props.showDate[0], this.props.showDate[1] + 1, this.props.showDate[2]];
                } else {
                    newDate = [this.props.showDate[0] + 1, 1, this.props.showDate[2]];
                }
                this.setState({ daysRollStatus: 0 });
                this.props.onShowDateChange(newDate);
            }, 300);
        });
    }

    calculateDaysList() {
        const { startDate, endDate, selectedDate, showDate } = this.props;
        const useDate = new PersianDate(showDate);
        const daysList = [];

        // two months earlier
        let endOfMonthWeekday = useDate.subtract('months', 2).endOf('month').day();
        let endOfMonthDay = useDate.subtract('months', 2).endOf('month').date();
        for (let i = endOfMonthDay - endOfMonthWeekday + 1; i <= endOfMonthDay; i++) {
            daysList.push({ number: i, state: DAY_STATE.INACTIVE });
        }

        // one month earlier
        endOfMonthDay = useDate.subtract('months', 1).endOf('month').date();
        for (let i = 1; i <= endOfMonthDay; i++) {
            daysList.push({ number: i, state: DAY_STATE.INACTIVE });
        }

        // current month
        endOfMonthDay = useDate.endOf('month').date();
        for (let i = 1; i <= endOfMonthDay; i++) {
            let state = DAY_STATE.IN_RANGE;
            useDate.date(i); // set date to today
            if (startDate && DatePlus.compare([useDate.year(), useDate.month(), useDate.date()], startDate) < 0) {
                state = DAY_STATE.NOT_IN_RANGE;
            }
            if (endDate && DatePlus.compare([useDate.year(), useDate.month(), useDate.date()], endDate) > 0) {
                state = DAY_STATE.NOT_IN_RANGE;
            }
            if (selectedDate && DatePlus.compare([useDate.year(), useDate.month(), useDate.date()], selectedDate) === 0) {
                state = DAY_STATE.SELECTED;
            }
            daysList.push({ number: i, state: state });
        }

        // one month later
        endOfMonthDay = useDate.add('months', 1).endOf('month').date();
        for (let i = 1; i <= endOfMonthDay; i++) {
            daysList.push({ number: i, state: DAY_STATE.INACTIVE });
        }

        // two months later
        let startOfMonthWeekday = useDate.add('months', 2).startOf('month').day();
        for (let i = 1; i <= 7 - startOfMonthWeekday + 1; i++) {
            daysList.push({ number: i, state: DAY_STATE.INACTIVE });
        }

        return daysList;
    }

    renderDays(daysList) {
        const dayRowItems = [];

        for (let i = 0; i < (daysList.length / 7); i++) {
            let dayItems = [];
            for (let j = 0; j < 7; j++) {
                const index = (i * 7) + j;
                let itemClassName = '';
                let itemStyle = {};
                if (daysList[index].state === DAY_STATE.IN_RANGE) { itemStyle = { color: this.props.secondaryColor, cursor: 'pointer' }; }
                else if (daysList[index].state === DAY_STATE.NOT_IN_RANGE) { itemStyle = { color: Color.addAlpha(this.props.secondaryColor, 0.5) }; }
                else if (daysList[index].state === DAY_STATE.SELECTED) { itemStyle = { backgroundColor: this.props.secondaryColor, color: this.props.primaryColor }; }
                else { itemStyle = { color: Color.addAlpha(this.props.secondaryColor, 0.2) }; }
                dayItems.push(
                    <div
                        key={`i-${j}`}
                        className={`${this.props.className}-main-day-item ${itemClassName} ${this.props.small ? 'small' : ''}`}
                        style={itemStyle}
                        onClick={() => {
                            if (daysList[index].state === DAY_STATE.IN_RANGE) {
                                this.props.onSelectedDateChange([this.props.showDate[0], this.props.showDate[1], daysList[index].number]);
                            }
                        }}
                    >
                        {daysList[index].number}
                    </div>
                );
            }
            dayRowItems.push(
                <div
                    key={`r-${i}`}
                    className={`${this.props.className}-main-days-row ${this.props.small ? 'small' : ''}`}
                    style={{ flexDirection: this.props.gregorian ? 'row' : 'row-reverse' }}
                >
                    {dayItems}
                </div>
            );
        }

        return dayRowItems;
    }

    render() {
        const daysList = this.calculateDaysList();

        let mainDaysRollStyle = {};
        let unitSize = 40;
        if (this.props.small) { unitSize = 30; }

        if (this.state.daysRollStatus === 0) {
            let numberHit1 = 0;
            for (let i = 0; i < daysList.length; i++) {
                if (daysList[i].number === 1) {
                    numberHit1++;
                    if (numberHit1 === 2) {
                        mainDaysRollStyle = { marginTop: -(parseInt(i / 7) * unitSize) + 'px' };
                        break;
                    }
                }
    
            }
        } else if (this.state.daysRollStatus === 1) {
            mainDaysRollStyle = { marginTop: 0, transition: 'all 0.3s ease' };
        } else if (this.state.daysRollStatus === 2) {
            let numberHit1 = 0;
            for (let i = 0; i < daysList.length; i++) {
                if (daysList[i].number === 1) {
                    numberHit1++;
                    if (numberHit1 === 3) {
                        mainDaysRollStyle = { marginTop: -(parseInt(i / 7) * unitSize) + 'px', transition: 'all 0.3s ease' };
                        break;
                    }
                }
    
            }
        }

        return (
            <div className={`${this.props.className}-main`}>
                <div className={`${this.props.className}-main-top`}>
                    <div className={`${this.props.className}-main-date`} style={{ color: this.props.secondaryColor }}>
                        <span className={`${this.props.className}-main-date-year ${this.props.small ? 'small' : ''}`} onClick={() => { this.props.onShowStatusChange(2); }}>{this.props.showDate[0]}</span>
                        <span className={`${this.props.className}-main-date-month ${this.props.small ? 'small' : ''}`} onClick={() => { this.props.onShowStatusChange(1); }}>{PersianDate.rangeName().months[this.props.showDate[1] - 1]}</span>
                    </div>

                    <div className={`${this.props.className}-main-top-actions`}>
                        <div
                            className={`${this.props.className}-main-top-month-previous`}
                            style={{ color: this.props.secondaryColor }}
                            onClick={this.onMainTopMonthPreviousClick}
                        >
                            { (this.props.monthNavigators && this.props.monthNavigators.left) ?
                                this.props.monthNavigators.left
                                :
                                <span>&lsaquo;</span>
                            }
                        </div>

                        <div
                            className={`${this.props.className}-main-top-month-next`}
                            style={{ color: this.props.secondaryColor }}
                            onClick={this.onMainTopMonthNextClick}
                        >
                            { (this.props.monthNavigators && this.props.monthNavigators.right) ?
                                this.props.monthNavigators.right
                                :
                                <span>&rsaquo;</span>
                            }
                        </div>
                    </div>
                </div>

                <div className={`${this.props.className}-main-bottom ${this.props.small ? 'small' : ''}`}>
                    <div
                        className={`${this.props.className}-main-week-names-container ${this.props.small ? 'small' : ''}`}
                        style={{ flexDirection: this.props.gregorian ? 'row' : 'row-reverse' }}
                    >
                        {
                            PersianDate.rangeName().weekdaysMin.map((item, index) => {
                                return (
                                    <div
                                        key={index}
                                        className={`${this.props.className}-main-week-name ${this.props.small ? 'small' : ''}`}
                                        style={{ color: this.props.secondaryColor }}
                                    >
                                        {item}
                                    </div>
                                );
                            })

                        }
                    </div>

                    <div className={`${this.props.className}-main-days-container ${this.props.small ? 'small' : ''}`}>
                        <div className={`${this.props.className}-main-days-roll ${this.props.small ? 'small' : ''}`} style={mainDaysRollStyle}>
                            {this.renderDays(daysList)}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Main.propTypes = {
    small     : PropTypes.bool,
    gregorian : PropTypes.bool,

    startDate            : PropTypes.array,
    endDate              : PropTypes.array,
    selectedDate         : PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
    onSelectedDateChange : PropTypes.func,
    showDate             : PropTypes.array,
    onShowDateChange     : PropTypes.func,

    monthNavigators : PropTypes.object,
    
    onShowStatusChange : PropTypes.func,
    
    className      : PropTypes.string,
    primaryColor   : PropTypes.string,
    secondaryColor : PropTypes.string,

    onChange     : PropTypes.func,
};

export { Main };
