
function compare(date1, date2) {
    if (date1[0] > date2[0]) { // year
        return 1;
    } else if (date1[0] < date2[0]) {
        return -1;
    } else { // equal year
        if (date1[1] > date2[1]) { // month
            return 1;
        } else if (date1[1] < date2[1]) {
            return -1;
        } else {
            if (date1[2] > date2[2]) { // day
                return 1;
            } else if (date1[2] < date2[2]) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}

const DatePlus = {
    compare
};

export { DatePlus };
