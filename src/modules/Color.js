
function addAlpha(color, alpha) {
    if (color.includes('#')) {
        if (color.length === 4 || color.length === 7) {
            let newAlpha = parseInt(alpha * 255).toString(16);
            if (newAlpha.length === 1) {
                newAlpha = '0' + newAlpha;
            }
            return color + newAlpha;
        }
        return color;
    } else if (color.includes('rgb')) {
        if (!color.includes('rgba')) {
            return color.substr(0, 3) + 'a' + color.substr(3, color.length - 1 - 3) + ', ' + alpha + ')';
        }
        return color;
    }
    return color;
}

const Color = {
    addAlpha
};

export { Color };
